import SpotPage from 'containers/spot';
import { customSpotPageGetServerSideProps } from 'containers/spot/props';

export default SpotPage;

export const getServerSideProps = customSpotPageGetServerSideProps;
