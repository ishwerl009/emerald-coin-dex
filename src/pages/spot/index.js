import SpotPage from 'containers/spot';
import { spotPageGetServerSideProps } from 'containers/spot/props';

export default SpotPage;

export const getServerSideProps = spotPageGetServerSideProps;
