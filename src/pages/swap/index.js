import SwapPage from 'containers/swap';
import { swapPageGetServerSideProps } from 'containers/swap/props';

export default SwapPage;

export const getServerSideProps = swapPageGetServerSideProps;
